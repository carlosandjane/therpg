﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(CharacterControler))]
public class PlayerMotor_old: MonoBehaviour {

    //chosen target
    Transform target;
    //reference to NMA
    NavMeshAgent agent;


	// Use this for initialization
	void Start () {
        agent = GetComponent<NavMeshAgent>();
        GetComponent<CharacterControler>().onFocusChangedCallback += OnFocusChanged;

	}
    public void MoveToPoint(Vector3 point)
    {
        agent.SetDestination(point);
    }
    void OnFocusChanged(Interaction newFocus)
    {
        if (newFocus != null)
        {
            agent.stoppingDistance = newFocus.radius * .8f;
            agent.updateRotation = false;

            target = newFocus.interactionTransform;
        }
        else
        {
            agent.stoppingDistance = 0f;
            agent.updateRotation = true;
            target = null;
        }
    }
}
