﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(PlayerMotor_old))]
public class CharacterControler : MonoBehaviour {

    public delegate void OnFocusChanged(Interaction newFocus);
    public OnFocusChanged onFocusChangedCallback;

    //Variables
    Camera cam;
    PlayerMotor motor;
    public LayerMask movementMask;
    private float xDir;
    private float yDir;
    private float zDir;
    private float modHorizontal;
    private float modVertical;
    private float modDir;
    private float modRot;
    public float moveSpeed;
    public Interaction focus;

    public GameObject character;
    public Animator myAnim;

	void Start () {

        cam = Camera.main;
        motor = GetComponent<PlayerMotor>();
		
	}
	
	void Update () {
        modHorizontal = Input.GetAxis("Horizontal");
        modVertical = Input.GetAxis("Vertical");

        if (modHorizontal != 0)
        {
            //left and right movement
            xDir += modHorizontal*moveSpeed;
            myAnim.SetBool("isWalking", true);

        }
        if (modVertical != 0)
        {
            //up and down movement
            zDir += modVertical*moveSpeed;
            myAnim.SetBool("isWalking", true);
        }
   
        character.transform.position = new Vector3(xDir, yDir, zDir);
        //carlos.transform.rotation = Quaternion.Euler(xRot, yRot, zRot);
        if((modHorizontal + modVertical) == 0)
        {
            //no vertical or horizontal movement
           myAnim.SetBool("isWalking", false);
        }
        
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            //If the ray hits
            if (Physics.Raycast(ray, out hit, 100, movementMask))
            {
                motor.MoveToPoint(hit.point);               //Move to where we hit

                RemoveFocus();
            }
        }
                              
        //If we press right mouse
        if (Input.GetMouseButtonDown(1))
        {
            //We Create a ray
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            //If the ray hits
            if (Physics.Raycast(ray, out hit, 100))
            {
                Interaction interactable = hit.collider.GetComponent<Interaction>();
                if (interactable != null)
                {
                    SetFocus(interactable);
                }
            }
        }
    }

    // Set our focus to a new focus
    void SetFocus(Interaction newFocus)
    {
        if (onFocusChangedCallback != null)
            onFocusChangedCallback.Invoke(newFocus);

        // If our focus has changed
        if (focus != newFocus && focus != null)
        {
            // Let our previous focus know that it's no longer being focused
            focus.OnDefocused();
        }

        // Set our focus to what we hit
        // If it's not an interactable, simply set it to null
        focus = newFocus;

        if (focus != null)
        {
            // Let our focus know that it's being focused
            focus.OnFocused(transform);
        }

    }

    void RemoveFocus ()
    {
        focus = null;
    }
}
