﻿using UnityEngine;

public class Interaction : MonoBehaviour {

    public float radius = 3f;
    public Transform interactionTransform;

    void OnDrawGizmosSelected ()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
    public void OnDefocused()
    {

    }
    public void OnFocused(Transform transform)
    {

    }
	
}
